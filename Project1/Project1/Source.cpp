//Marcin Rusin 047922
#include <iostream>
#include <string>
using namespace std;

class atrakcja
{
protected:
	int cena;
	string nazwa;
	string opis;
public:
	int Cena()
	{
		return cena;
	}
	string Nazwa()
	{
		return nazwa;
	}
	string opis()
	{
		return opis;
	}
};

class kolejka : atrakcja
{
private:
	float godz_odjazdu;
	float godz_przyjazdu;
public:
	float odjazd()
	{
		return godz_odjazdu;
	}
	float przyjazd()
	{
		return godz_przyjazdu;
	}
	void inicjuj(float &odj,float &przyj)
	{
		godz_odjazdu=odj;
		godz_przyjazdu=przyj;
	}
};

class zamek : atrakcja
{
private:
	int czas_zwiedzania;
public:
	int czas()
	{
		return czas_zwiedzania;
	}
	void inicjuj(int &c)
	{
		czas_zwiedzania=c;
	}
};

class film : atrakcja
{
private:
	int czas_trwania;
	string tytul;
public:
	int dlugosc()
	{
		return czas_trwania;
	}
	string Tytul()
	{
		return tytul;
	}
	void inicjuj(int &dl,string &tyt)
	{
		czas_trwania=dl;
		tytul=tyt;
	}
};




int main()
{
	system("pause");
	return 0;
}